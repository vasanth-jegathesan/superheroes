package com.vj.superheroesrating.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.vj.superheroesrating.R;
import com.vj.superheroesrating.model.Hero;
import com.vj.superheroesrating.model.JsonKeys;
import com.vj.superheroesrating.presenter.SwipeListAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * Created by Vasanth Jegathesan
 */
public class MainActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {
    private static final String TAG = MainActivity.class.getSimpleName();

    public static String BASE_URL = "https://bitbucket.org/dttden/mobile-coding-challenge/raw/2ee8bd47703c62c5d217d9fb9e0306922a34e581/";

    private String[] dataYears = {"2006", "2009", "2012", "2015", "2018"};
    private static int currentYearIndex = 0;
    private static final int HEROES_LIMIT = 3;

    private SwipeRefreshLayout swipeRefreshLayout;
    private ListView listView;
    private SwipeListAdapter adapter;
    private List<Hero> heroList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Hiding ActionBar as shown in the requirement document
        Objects.requireNonNull(getSupportActionBar()).hide();

        setContentView(R.layout.activity_main);

        listView = findViewById(R.id.list_view);
        swipeRefreshLayout = findViewById(R.id.swipe_refresh_layout);

        heroList = new ArrayList<>();
        adapter = new SwipeListAdapter(this, heroList);
        listView.setAdapter(adapter);

        swipeRefreshLayout.setOnRefreshListener(this);

        swipeRefreshLayout.post(() -> {
            swipeRefreshLayout.setRefreshing(true);
            retrieveData();
        });
    }

    @Override
    public void onRefresh() {
        if (adapter.getCount() >= HEROES_LIMIT) {
            heroList.clear();
            listView.setAdapter(null);
            listView.setAdapter(adapter);
        }
        retrieveData();
    }

    private void retrieveData() {
        swipeRefreshLayout.setRefreshing(true);

        if (currentYearIndex == dataYears.length) {
            currentYearIndex = 0;
        }

        String currentYear = dataYears[currentYearIndex++];
        ((TextView)findViewById(R.id.current_year)).setText(currentYear);

        String url = BASE_URL + currentYear + ".json";

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
            (Request.Method.GET, url, null, response -> {
                if (response.length() > 0) {
                    try {
                        JSONArray heroes = response.getJSONArray(JsonKeys.HEROES.getKey());

                        for (int i = 0; i < heroes.length(); i++) {
                            JSONObject hero = heroes.getJSONObject(i);
                            Hero h = new Hero(hero.getString(JsonKeys.NAME.getKey()),
                                    hero.getString(JsonKeys.PICTURE.getKey()),
                                    hero.getInt(JsonKeys.SCORE.getKey()));

                            heroList.add(i, h);
                        }

                        // Sort according to the Heroes score/popularity
                        Collections.sort(heroList, Collections.reverseOrder());

                        // Remove unnecessary memory
                        heroList.subList(HEROES_LIMIT, heroList.size()).clear();
                    } catch (JSONException e) {
                        Log.e(TAG, "JSON parsing error: " + e.getMessage());
                    }

                    adapter.notifyDataSetChanged();
                }

                // Stop swipe refresh
                swipeRefreshLayout.setRefreshing(false);
            }, error -> {
                Toast.makeText(this, "No connection", Toast.LENGTH_LONG).show();

                Log.e(TAG, "HTTP error: " + error.getMessage());
                swipeRefreshLayout.setRefreshing(false);
            });

        RequestSingleton.getInstance().addToRequestQueue(this, jsonObjectRequest);
    }
}
