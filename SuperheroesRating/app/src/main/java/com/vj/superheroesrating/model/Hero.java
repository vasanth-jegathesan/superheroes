package com.vj.superheroesrating.model;

/**
 * Created by Vasanth Jegathesan
 */
public class Hero implements Comparable<Hero> {
    // Can be encapsulated with getter/setter methods - leaving as it is a simple POJO
    public String name;
    public String picture;
    public Integer score;

    public Hero(String name, String picture, int score) {
        this.name = name;
        this.picture = picture;
        this.score = score;
    }

    @Override
    public int compareTo(Hero h) {
        return this.score.compareTo(h.score);
    }
}
