package com.vj.superheroesrating.view;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by Vasanth Jegathesan
 */
class RequestSingleton {
    private static final String TAG = RequestSingleton.class.getSimpleName();

    private RequestQueue mRequestQueue;

    private RequestSingleton() {}

    private static class SingletonHelper {
        private static final RequestSingleton REQUEST_SINGLETON = new RequestSingleton();
    }

    static RequestSingleton getInstance() {
        return SingletonHelper.REQUEST_SINGLETON;
    }

    <T> void addToRequestQueue(Context context, Request<T> req) {
        req.setTag(TAG);
        getRequestQueue(context).add(req);
    }

    private RequestQueue getRequestQueue(Context context) {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(context);
        }

        return mRequestQueue;
    }
}
