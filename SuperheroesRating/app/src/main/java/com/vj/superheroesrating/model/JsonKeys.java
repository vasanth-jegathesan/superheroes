package com.vj.superheroesrating.model;

/**
 * Created by Vasanth Jegathesan
 */
// All JSON keys are maintained in one place
public enum JsonKeys {
    HEROES("Heroes"),
    NAME("Name"),
    PICTURE("Picture"),
    SCORE("Score");

    private String key;

    JsonKeys(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }
}
