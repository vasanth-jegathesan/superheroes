package com.vj.superheroesrating.presenter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.Volley;
import com.vj.superheroesrating.R;
import com.vj.superheroesrating.model.Hero;
import com.vj.superheroesrating.view.MainActivity;

import java.util.List;

/**
 * Created by Vasanth Jegathesan
 */
public class SwipeListAdapter extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private List<Hero> heroList;

    public SwipeListAdapter(Activity activity, List<Hero> heroList) {
        this.activity = activity;
        this.heroList = heroList;
    }

    @Override
    public int getCount() {
        return heroList.size();
    }

    @Override
    public Object getItem(int position) {
        return heroList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (inflater == null)
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.table, null);

        ImageView superheroImageView = convertView.findViewById(R.id.superhero_image);
        superheroImageView.setAdjustViewBounds(true);

        TextView superheroName = convertView.findViewById(R.id.superhero_name);
        TextView superheroScore = convertView.findViewById(R.id.superhero_score);

        String imageUrl = MainActivity.BASE_URL + heroList.get(position).picture
                                            .replace("jpeg", "jpg");

        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        final ImageRequest imageRequest = new ImageRequest(imageUrl, superheroImageView::setImageBitmap,
                0,0, ImageView.ScaleType.CENTER_CROP,null,
                Throwable::printStackTrace);
        requestQueue.add(imageRequest);

        superheroName.setText(heroList.get(position).name);
        superheroScore.setText(String.valueOf(heroList.get(position).score));

        return convertView;
    }
}
